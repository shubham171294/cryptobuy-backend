var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
  var response = {
    status: 200,
    data: [{ name: 'Shubham', age: 2 }, { name: 'test', age: 22 }]
  };
  res.send(JSON.stringify(response));
});

module.exports = router;
