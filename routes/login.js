var express = require('express');
var router = express.Router();

/* GET users listing. */
router.post('/', function (req, res, next) {
    var username = req.body.username;
    var password = req.body.password;
    var response;
    if (username === "ABC" && password === "123") {
        response = {
            status: 200,
            success : true,
            message: "Login Successful"
        };
    } else {
        response = {
            status: 200,
            success : false,
            error: "Wrong credentials. Username should be Abc and password should be 123"
        };
    }
    res.send(response);
});

module.exports = router;
